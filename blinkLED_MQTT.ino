#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// --------- GPIO --------
#define D0    16
#define D1    5
#define D2    4
#define D3    0
#define D4    2
#define D5    14
#define D6    12
#define D7    13
#define D8    15
#define D9    3
#define D10   1

// --------- MQTT TOPICS --------
#define SUBSCRIBE_TOPIC   "que_envia"
#define PUBLISH_TOPIC     "que_recebe"

// --------- MQTT ID --------
#define ID_MQTT   "mqtt_test"

// --------- Wi-Fi --------
const char* SSID = "ssid";
const char* PASSWORD = "passwd";

// --------- MQTT --------
const char* BROKER_MQTT = "m11.cloudmqtt.com";	// URL do broker MQTT
int BROKER_PORT = 16163;			// Porta do Broker MQTT
const char* user = "user";			// usuário do cloudMQTT
const char* passwd = "passwd";			// senha do cloudMQTT

WiFiClient espClient;
PubSubClient MQTT(espClient);
char outputState = '0';

// --------- Prototypes --------
void initSerial();
void initWifi();
void initMQTT();
void reconnectWiFi();
void reconnectMQTT();
void mqtt_callback(char* topic, byte* payload, unsigned int length);
void isConnect();
void initOutput();
void sendState();

void setup() {
  set_leds();
  initOutput();
  initSerial();
  initWiFi();
  initMQTT();
}

void initSerial() {
  Serial.begin(115200);
}

void initWiFi() {
  delay(10);
  Serial.println("------Conexao WI-FI------");
  Serial.print("Conectando-se na rede: ");
  Serial.println(SSID);
  Serial.println("Aguarde");
     
  reconnectWiFi();
}

void initMQTT() {
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  MQTT.setCallback(mqtt_callback);
}

void mqtt_callback(char* topic, byte* payload, unsigned int length) {
  String msg;
  for(int i = 0; i < length; ++i) {
    char c = (char)payload[i];
    msg += c;
  }

  if (msg.equals("L")) {
    digitalWrite(D0, HIGH);
    outputState = 1;
  }
  if (msg.equals("D")) {
    digitalWrite(D0, LOW);
    outputState = 0;
  }
}

void reconnectWiFi() {
  if(WiFi.status() == WL_CONNECTED) {
    return;
  }
  WiFi.begin(SSID, PASSWORD);

  while(WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Conectado com suscesso na rede ");
  Serial.print(SSID);
  Serial.println("IP obtido: ");
  Serial.println(WiFi.localIP());
}

void reconnectMQTT() {
  while(!MQTT.connected()) {
    Serial.print("* Tendando se conectar ao Broker MQTT: ");
    Serial.println(BROKER_MQTT);
    if(MQTT.connect(ID_MQTT, user, passwd)) {
      Serial.println("Conectado com sucesso ao broker MQTT");
      MQTT.subscribe(SUBSCRIBE_TOPIC);
    }
    else {
      Serial.println("Falha ao conectar no broker MQTT");
      Serial.println("Haverá uma nova tentativa em 2s");
      delay(2000);
    }
  }
}

void isConnect() {
  if(!MQTT.connected()) {
    reconnectMQTT();
  }
  reconnectWiFi();
}

void sendState() {
  if(outputState == '0') {
    MQTT.publish(PUBLISH_TOPIC, "D");
  }
  if(outputState == '1') {
    MQTT.publish(PUBLISH_TOPIC, "L");
  }
  Serial.println(" - Estado de saída D0 enviado ao broker!");
  delay(1000);
}

void initOutput() {
  pinMode(D0, OUTPUT);
}

void set_leds() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(D8, OUTPUT);
  pinMode(D9, OUTPUT);
  pinMode(D10, OUTPUT);
}

void blink_all() {
  // put your main code here, to run repeatedly:
  digitalWrite(D0, !digitalRead(D0));
  digitalWrite(D1, !digitalRead(D1));
  digitalWrite(D2, !digitalRead(D2));
  digitalWrite(D3, !digitalRead(D3));
  digitalWrite(D4, !digitalRead(D4));
  digitalWrite(D5, !digitalRead(D5));
  digitalWrite(D6, !digitalRead(D6));
  digitalWrite(D7, !digitalRead(D7));
  digitalWrite(D8, !digitalRead(D8));
  digitalWrite(D9, !digitalRead(D9));
  digitalWrite(D10, !digitalRead(D10));
  delay(1000);
}

void loop() {
  isConnect();
  sendState();
  MQTT.loop();
}
